# oh-my-plom

## Getting started

1. Run `sh -c "$(curl -fsSL "https://gitlab.com/api/v4/projects/34235533/repository/files/install.sh/raw?ref=main")"`
2. Configure environment variables in `~/.oh-my-plom/plom.plugin.sh`
3. Start a new terminal session
4. Profit
