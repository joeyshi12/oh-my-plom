#!/bin/bash

PLUGIN="~/.oh-my-plom/plom.plugin.sh"
REPO="joeyshi12/oh-my-plom"
REMOTE="https://gitlab.com/${REPO}.git"

command_exists() {
  command -v "$@" >/dev/null 2>&1
}

fmt_error() {
  printf '%sError: %s%s\n' "\033[1m\033[31m" "$*" "\033[0m" >&2
}

# $1: shell rc file path
source_plom() {
    echo -e "\n[ -f "$PLUGIN" ] && source "$PLUGIN"" >> "$1"
}

command_exists git || {
    fmt_error "git is not installed"
    exit 1
}

git clone "$REMOTE" ~/.oh-my-plom

case "$SHELL" in
    *zsh) source_plom ~/.zshrc;;
    *bash) source_plom ~/.bashrc;;
    *) fmt_error "could not identify shell; source $PLUGIN in your shell\'s rc file";;
esac
