export PLOM_SERVER=localhost
export PLOM_MANAGER_PASSWORD=1234
export PLOM_NO_SSL_VERIFY=1

PLOM_WORK_DIR=~/Workspace

plom() {
    case $1 in
        create|server|client)
            python3 $PLOM_WORK_DIR/plom/plom/$1 ${@:2}
            ;;
        demo)
            python3 $PLOM_WORK_DIR/plom/plom/scripts/demo.py
            ;;
        demo-server)
            sudo docker run -it --rm\
                -p 41985:41984 plomgrading/server bash\
                -c "PLOM_NO_SSL_VERIFY=1 plom-demo"
            ;;
        cp-demo)
            local container_id="$(docker ps | grep "plomgrading/server" | awk "{print $1}")"
            if [ -z $container_id ]; then
                echo "Demo server needs to be running; try running plom demo-server"
            else
                docker cp $container_id:/exam/. $PLOM_WORK_DIR
            fi
            ;;
        *)
            echo "Valid options: create, server, client, demo, demo-server, cp-demo"
            ;;
    esac
}
